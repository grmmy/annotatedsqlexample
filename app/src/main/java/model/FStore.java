package model;

import com.annotatedsql.annotation.provider.Provider;
import com.annotatedsql.annotation.sql.Schema;

/**
 * Created by Anton on 6/26/2014.
 */
@Schema(className="FSchema", dbName="football.db", dbVersion=1)
@Provider(authority="com.gdubina.football.providers.rss", schemaClass="FSchema", name="FProvider")
public interface FStore {}
package model;

import com.annotatedsql.annotation.provider.URI;
import com.annotatedsql.annotation.sql.Column;
import com.annotatedsql.annotation.sql.PrimaryKey;
import com.annotatedsql.annotation.sql.Table;

/**
 * Created by Anton on 6/26/2014.
 */
@Table(TeamTable.TABLE_NAME)
public interface TeamTable{

    String TABLE_NAME = "team_table";

    @URI
    String CONTENT_URI = "team_table";

    @PrimaryKey
    @Column(type = Column.Type.INTEGER)
    String ID = "_id";

    @Column(type = Column.Type.TEXT)
    String TITLE = "title";

    @Column(type = Column.Type.INTEGER)
    String CHEMP_ID = "chemp_id";
}

package example.annotadedsql;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import model.FProvider;
import model.TeamTable;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends ListFragment {

        private SimpleCursorAdapter mAdapter;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View result = super.onCreateView(inflater, container, savedInstanceState);
            Cursor c = getActivity().getContentResolver().query(FProvider.getContentUri(TeamTable.CONTENT_URI), new String[]{TeamTable.ID, TeamTable.TITLE}, null, null, null);
            FProvider provider = new FProvider();
//            Cursor c = provider.query(FProvider.getContentUri(TeamTable.CONTENT_URI), new String[]{TeamTable.ID, TeamTable.TITLE}, null, new String[]{""}, null);
            Toast.makeText(getActivity(), "C = " + c.getCount(), Toast.LENGTH_SHORT).show();
            mAdapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1, null, new String[]{TeamTable.TITLE}, new int[]{android.R.id.text1});
            setListAdapter(mAdapter);
            getLoaderManager().initLoader(0, null, new LoaderManager.LoaderCallbacks<Cursor>() {
                @Override
                public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                    return new CursorLoader(getActivity(), FProvider.getContentUri(TeamTable.CONTENT_URI), new String[]{TeamTable.ID, TeamTable.TITLE}, null, null, null);
                }

                @Override
                public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                    mAdapter.swapCursor(data);
                }

                @Override
                public void onLoaderReset(Loader<Cursor> loader) {
                    mAdapter.swapCursor(null);
                }
            });
            return result;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            getListView().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ContentValues cv = new ContentValues();
                    cv.put(TeamTable.TITLE, "Lol");
                    getActivity().getContentResolver().insert(FProvider.getContentUri(TeamTable.CONTENT_URI), cv);
                    getListView().postDelayed(this, 1000);
                }
            }, 1000);
        }
    }
}
